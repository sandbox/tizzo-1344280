<?php

$plugin = array(
  // As this is the base class plugin, it shouldn't declare any menu items.
  'schema' => 'relation_queue',
  'access' => 'administer relation queue',

  'menu' => array(
    'menu prefix' => 'admin/structure',
    'menu item' => 'relation-queue',
    'menu title' => 'Relation Queues', 
    'menu description' => 'Create and maintain simple relation queues',
  ),

  'title singular' => t('relation queue'),
  'title singular proper' => t('Relation Queue'),
  'title plural' => t('relation queues'),
  'title plural proper' => t('Relation Queues'),
  'handler' => array(
    'class' => 'relation_queue_export_ui',
  ),

  'allowed operations' => array(
    'view' => array(
      'title' => 'View',
    ),
  ),
);
