
<?php

/**
 * Base class for export UI.
 */
class relation_queue_export_ui extends ctools_export_ui {

  /**
   * Implements ctools_export_ui::init().
   *
   * Add our view menu item to the path.
   */
  function init($plugin) {
    parent::init($plugin);
    $menu_items = &$this->plugin['menu']['items'];
    $menu_items['view'] = $menu_items['add'];
    $menu_items['view']['path'] = 'list/%ctools_export_ui/view';
    $menu_items['view']['title'] = 'View';
    $menu_items['view']['page arguments'] = array('relation_queue_export_ui', 'view');
    $menu_items['view']['access arguments'] = array('relation_queue_export_ui', 'view');
  }

  function view_page($js, $input, $step = NULL) {
    // TODO: There may be an easier way to get this?
    $queue = relation_queue_load_queue(arg(4));
    $results = relation_query('relation_queue', $queue->id)->execute();
    $items = entity_load('relation', array_keys($results));
    foreach ($items as $item) {
      $type = $item->endpoints[LANGUAGE_NONE][0]['entity_type'];
      $id = $item->endpoints[LANGUAGE_NONE][0]['entity_id'];
      $entities[$type][] = $id;
    }
    $rows = array();
    $delta = 0;
    foreach ($entities as $type => $ids) {
      $loaded_entities[$type] = entity_load($type, $ids);
      foreach ($loaded_entities[$type] as $entity) {
        $rows[$type . '_' . $id] = array(
          'class' => array('draggable'),
          'data' => array(
            $id,
            $type,
            entity_label($type, $entity),
          ),
        );
        $delta++;
      }
    }
    $page = array();
    $page['queued_items'] = array(
      '#theme' => 'table',
      '#header' => array(t('ID'), t('Type'), t('Label')),
      '#rows' => $rows,
      '#attributes' => array('id' => 'my-module-table'),
    );
    $page['funk'] = array(
      '#markup' => '<p>We need to be addin\' some stuff here...</p>',
    );
    return $page;
  }

  function edit_form(&$form, &$form_state) {
    parent::edit_form($form, $form_state);
    unset($form['info']['name']);
    $form['info']['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Queue name'),
      '#required' => TRUE,
      '#default_value' => '',
      '#size' => 32,
      '#maxlength' => 255,
    );
    if ($form_state['item']->name == '') {
      $form['info']['name'] = array(
        '#title' => t('View name'),
        '#type' => 'machine_name',
        '#required' => TRUE,
        '#maxlength' => 32,
        '#size' => 32,
        '#machine_name' => array(
          'exists' => 'ctools_export_ui_edit_name_exists',
          'source' => array('info', 'label'),
        ),
      );
    }
    $form['info']['description'] = array(
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => '',
      '#description' => t('Describe the use of this queue within the system.'),
    );
    if (isset($form_state['item'])) {
      $queue = $form_state['item'];
      foreach ($form['info'] as $element => &$item) {
        if (isset($queue->$element) && isset($item['#default_value'])) {
          $item['#default_value'] = $queue->$element;
        }
      }
    }
  }

}

